import json
import click


def main(path):
    with open(path, "r") as f:
        counts = json.load(f)

    counts = counts["ClassifiedMalignantLesions"]

    tot_rf = 0
    tot_ci = 0
    for k, v in counts.items():
        if "R-" in k or "F-" in k:
            tot_rf += v
        elif "CI" in k:
            tot_ci += v

    rows = (
        "Invasive-G1",
        "Invasive-G2",
        "Invasive-G3",
        "Invasive-NA",
        "Invasive-NoGrade",
        "Insitu-NDL",
        "Insitu-NDI",
        "Insitu-NDH",
        "Insitu-NA",
        "Insitu-NoGrade",
    )

    # Merge R and F types
    ep_types = ("R", "F")
    joint_key = "R&F"
    for ep_type in ep_types:
        for row in rows:
            key = f"{joint_key}-{row}"
            counts[key] = counts.get(key, 0) + counts.get(f"{ep_type}-{row}", 0)

    for ep_type, totals in zip(("R&F", "CI"), (tot_rf, tot_ci)):
        total = 0
        for row in rows:
            entry = f"{ep_type}-{row}"
            count = counts.get(entry, 0)
            print(f"{entry} \t: {count}")
            total += count

        print(f"Total \t:{total}")

        print(f"Excluded: {totals - total}")


@click.command()
@click.argument("path-to-counts")
def cli(path_to_counts):
    main(path_to_counts)


if __name__ == "__main__":

    cli()
