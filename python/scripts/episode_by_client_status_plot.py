import click
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json
from matplotlib import rc
import matplotlib


def main(path, output):

    with open(path, "r") as f:
        counts = json.load(f)

    df = pd.DataFrame(counts["ClientEpisode"])
    df.index = df.index.astype("int")

    print(df)

    df.loc[3] = df.loc[df.index >= 3].sum()
    df = df.loc[[1, 2, 3]]

    sns.set_style("ticks", {"font.family": "serif", "font.sans-serif": "Arial"})
    rc("font", **{"family": "sans-serif", "sans-serif": ["Arial"]})
    colors = sns.color_palette("Blues_d")

    keys = [
        ("normal", "(a) Normal"),
        ("interval", "(b) Interval cancer"),
        ("benign", "(c) Benign"),
        ("malignant", "(d) Malignant"),
    ]

    fig, axes = plt.subplots(2, 2, sharex=False, figsize=(7.0, 6))

    for i, key in enumerate(keys):
        j = i % 2
        i = 0 if i < 2 else 1
        ax = axes[i][j]
        sns.barplot(x=df.index, y=df[key[0]], ax=ax, color=colors[-1])

        if key[0] == "normal":
            pos = (0.02, 0.9)
        else:
            pos = (0.5, 0.9)
        ax.annotate(key[1], pos, xycoords="axes fraction", fontsize="large")

        ax.tick_params(axis="both", which="major", labelsize=10)

        tick_labels = np.arange(1, 4)
        tick_labels = [str(_) for _ in tick_labels]
        tick_labels[-1] = r"$\geq$" + str(tick_labels[-1])

        ax.set_xticklabels(tick_labels)
        ax.set_ylabel(None)

    sns.despine()

    plt.tight_layout()
    fig.text(
        0.55,
        0.02,
        "Number of screening episodes",
        va="center",
        ha="center",
        weight="bold",
        fontsize="x-large",
    )
    fig.text(
        0.02,
        0.5,
        "Number of women",
        va="center",
        ha="center",
        rotation=90,
        weight="bold",
        fontsize="x-large",
    )

    plt.subplots_adjust(bottom=0.1, left=0.12)
    plt.savefig(output, dpi=300)
    plt.show()


@click.command()
@click.argument("path-to-counts")
@click.argument("output")
def cli(path_to_counts, output):
    main(path_to_counts, output)


if __name__ == "__main__":

    cli()
