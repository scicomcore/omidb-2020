import click
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json
from matplotlib import rc
import matplotlib


def main(path):

    with open(path, "r") as f:
        data = json.load(f)

    others = 0
    high_risk = 0
    client_counts = data["ClientsWithScreeningImages"]
    for key, val in client_counts.items():
        print(key, val)
        if "HighRisk" in key:
            high_risk += val
        else:
            others += val
    total = sum(client_counts.values())
    print(f"Total number of women with R/F episodes and imaging: {others}")
    print(f"Total number of women with H episodes and imaging: {high_risk}")

    print("~~~Number of images~~~")
    total = 0
    for key, val in data["NumImages"].items():
        if "screening" not in key:
            total += val
    print(total)

    total_lesions = total_marked = 0
    for t in ("R", "F"):
        total_lesions += data["LesionsWithScreeningImaging"][t]
        total_marked += data["MarkedLesionsWithScreeningImaging"][t]
    print(f"Total cancers from R/F with images : {total_lesions}")
    print(f"Of which marked : {total_marked}")


@click.command()
@click.argument("path-to-counts")
def cli(path_to_counts):
    main(path_to_counts)


if __name__ == "__main__":

    cli()
