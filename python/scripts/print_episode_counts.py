import click
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json

def main(path):

    with open(path, 'r') as f:
        counts = json.load(f)
    counts = counts['episode-counts']

    keys = ('normal', 'interval', 'benign', 'malignant', 'interval')

    groups = (2, 3, 4)

    for key in keys:
        data = np.array(counts[key])

        for i in groups[:-1]:
            print(f'{key} == {i} \t:', np.sum(data == i))
        print(f'{key} >= {groups[-1]} \t:', np.sum(data >= groups[-1]))

@click.command()
@click.argument('path-to-counts')
def cli(path_to_counts):
    main(path_to_counts)


if __name__ == '__main__':

    cli()
