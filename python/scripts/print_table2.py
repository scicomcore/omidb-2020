import click
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json
from matplotlib import rc
import matplotlib


def hist(x):
    x = x[(x >= 30) & (x < 85)]
    return np.histogram(x, bins=11, range=(30, 85))


def main(path):

    with open(path, "r") as f:
        data = json.load(f)

    ages = data["Ages"]
    # Merge R and F
    ep_types = ("R", "F")
    key = "R&F"
    ages["R&F"] = ages["R"] + ages["F"]

    for group in ("R&F", "H"):
        x = np.array(ages[group])
        freqs, bins = hist(x)
        print(group)
        for bin, freq in zip(bins, freqs):
            print(bin, freq)
        print(f"total: {np.sum(freqs)}")


@click.command()
@click.argument("path-to-counts")
def cli(path_to_counts):
    main(path_to_counts)


if __name__ == "__main__":

    cli()
