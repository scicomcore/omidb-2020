import click
from loguru import logger
from omidb_paper_stats import count_funcs

logger.enable("omidb")


@click.command()
@click.argument("path-to-omidb")
@click.argument("clients", nargs=-1, default=None)
@click.option("--output", default=None)
def cli(path_to_omidb, clients, output):
    import json

    out = count_funcs.main(path_to_omidb, clients)

    if output:
        with open(output, "w") as f:
            json.dump(out, f)
    else:
        print(json.dumps(out, indent=4, sort_keys=True))


if __name__ == "__main__":
    cli()
