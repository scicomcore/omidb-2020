from collections import Counter
import omidb
from . import utils


def count_clients_with_num_screening_episodes(client, counter_dicts):

    client_status = client.status

    if client_status == omidb.client.Status.N:
        key = "normal"
        episodes = utils.get_all_screening_episodes(client)
    else:
        episodes = utils.get_abnormal_episodes(client)
        if not episodes:
            return

        episodes = utils.get_all_screening_episodes(client, episodes[-1][0])

        if client_status == omidb.client.Status.B:
            key = "benign"
        elif client_status == omidb.client.Status.M:
            key = "malignant"
        elif client_status == omidb.client.Status.CI:
            key = "interval"

    if episodes:
        if key in counter_dicts:
            counter_dicts[key].update([len(episodes)])
        else:
            counter_dicts[key] = Counter([len(episodes)])


def count_malignant_lesions(client, counter):
    classes = utils.classify_malignant_lesions_where_screening_imaging(client)
    if classes:
        counter.update(classes)


def num_marked_lesions_from_screening_images(
    client, lesion_counter, marked_lesion_counter
):
    """
    Iterates through all screening images (not assessment) associated with
    `client` and counts the number of lesions that have been marked. Only marks
    that are linkable to NBSS lesions are counted, and are assumed to be unique
    across episodes. Marks within an episode are considered distinct by their
    ID.
    """

    for episode in client.episodes:

        episode_lesions = utils.get_malignant_lesions(episode)

        if not episode_lesions:
            continue

        marked_lesion_ids = set()
        image = None
        for image in utils.screening_image_iter(episode):
            for mark in image.marks:
                if mark.lesion_ids:
                    marked_lesion_ids.update(mark.lesion_ids)

        if image:
            lesion_ids = set([_.id for _ in episode_lesions])
            lesion_counter.update({episode.type.name: len(lesion_ids)})
            if marked_lesion_ids:
                marked_lesion_counter.update(
                    {episode.type.name: len(marked_lesion_ids.intersection(lesion_ids))}
                )


def ages_where_screening_imaging(client, ages_dict):

    for episode in client.episodes:
        age = None
        for image in utils.screening_image_iter(episode):
            age = utils.parse_age(image)
            if age is not None:
                break

        if age is None:
            continue

        group = episode.type.name

        if group in ages_dict:
            ages_dict[group].append(age)
        else:
            ages_dict[group] = [age]


def num_images(client, counter):
    for ep in client.episodes:
        if not ep.studies:
            continue

        total = 0
        screening_images = 0

        for study in ep.studies:

            num_images = 0
            for series in study.series:
                num_images += series.num_images

            if not num_images:
                continue

            total += num_images

            if (
                study.event_type
                and (len(study.event_type) == 1)
                and (omidb.events.Event.screening in study.event_type)
            ):
                screening_images += num_images

        counter.update(
            {ep.type.name: total, f"{ep.type.name}-screening": screening_images}
        )


def count_clients(client, client_counts):

    client_status = client.status

    if client_status == omidb.client.Status.N:
        key = "normal"
    elif client_status == omidb.client.Status.B:
        key = "benign"
    elif client_status == omidb.client.Status.M:
        key = "malignant"
    elif client_status == omidb.client.Status.CI:
        key = "interval"

    if utils.client_has_high_risk_episode(client):
        key += "HighRisk"
        client_counts[key] = client_counts.get(key, 0) + 1
    elif utils.get_all_screening_episodes(client):
        client_counts[key] = client_counts.get(key, 0) + 1


def main(path_to_omidb, clients):

    client_episode_counters = {}
    client_counters = {}
    lesion_counter = Counter()
    marked_lesion_counter = Counter()
    malignant_lesion_counter = Counter()
    ages = {}
    image_counter = Counter()

    for client in omidb.DB(
        path_to_omidb, clients=clients, distinct_event_study_links=True
    ):

        count_malignant_lesions(client, malignant_lesion_counter)

        count_clients_with_num_screening_episodes(client, client_episode_counters)

        count_clients(client, client_counters)

        ages_where_screening_imaging(client, ages)

        num_marked_lesions_from_screening_images(
            client, lesion_counter, marked_lesion_counter
        )

        num_images(client, image_counter)

    return {
        "ClientEpisode": client_episode_counters,
        "ClientsWithScreeningImages": client_counters,
        "ClassifiedMalignantLesions": malignant_lesion_counter,
        "MarkedLesionsWithScreeningImaging": marked_lesion_counter,
        "LesionsWithScreeningImaging": lesion_counter,
        "Ages": ages,
        "NumImages": image_counter,
    }
