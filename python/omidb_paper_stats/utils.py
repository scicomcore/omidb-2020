import re
import dataclasses
import omidb
from omidb.events import Opinion


def screening_image_iter(episode):
    # We must have screening images, first filter
    if not (episode.events.screening and episode.studies):
        return
    for study in episode.studies:
        if (
            study.event_type
            and (len(study.event_type) == 1)
            and (omidb.events.Event.screening in study.event_type)
        ):
            for series in study.series:
                for image in series.images:
                    yield (image)


def episode_has_screening_images(episode):
    for im in screening_image_iter(episode):
        return True
    return False


def client_has_screening_images(client):
    for ep in client.episodes:
        if episode_has_screening_images(ep):
            return True
    return False


def get_abnormal_episodes(client, episode_condition=None):
    if episode_condition is None:
        episode_condition = default_condition(client)
    episodes = []
    for episode in client.episodes:
        date = get_earliest_date(episode)
        if not date:
            continue

        # store date If we have date and episode condition has been met
        if date and episode_condition(episode):
            episodes.append((date, episode))

    return sorted(episodes, key=lambda e: e[0])


def is_screening_episode(episode):
    if (
        episode.type in (omidb.episode.Type.R, omidb.episode.Type.F)
    ) and episode_has_screening_images(episode):
        return True
    return False


def is_high_risk_episode(episode):
    if episode.type == omidb.episode.Type.H and episode_has_screening_images(episode):
        return True
    return False


def client_has_high_risk_episode(client):
    for ep in client.episodes:
        if is_high_risk_episode(ep):
            return True
    return False


def get_all_screening_episodes(client, end_date=None):

    episodes = []
    for episode in client.episodes:

        if not is_screening_episode(episode):
            continue

        if episode.events.screening.dates:
            date = min(episode.events.screening.dates)
            if end_date:
                if date <= end_date:
                    episodes.append((date, episode))
            else:
                episodes.append((date, episode))

    return sorted(episodes, key=lambda e: e[0]) if episodes else None


def default_condition(client):
    client_status = client.status
    if client_status == omidb.client.Status.B:

        def episode_condition(e):
            return e.has_benign_opinions

    elif client_status == omidb.client.Status.M:

        def episode_condition(e):
            return e.has_malignant_opinions

    elif client_status == omidb.client.Status.CI:

        def episode_condition(e):
            return e.type == omidb.episode.Type.CI and e.has_malignant_opinions

    else:
        return None

    return episode_condition


def get_earliest_date(episode):

    dates = []
    for field in dataclasses.fields(episode.events):
        event = getattr(episode.events, field.name)
        if event and event.dates:
            dates += event.dates
    if dates:
        return min(dates)
    return None


def sort_episodes(client):

    episodes = []
    for episode in client.episodes:
        date = get_earliest_date(episode)
        if date:
            episodes.append((date, episode))
        else:
            return

    return sorted(episodes, key=lambda e: e[0])


def get_ci_episodes_with_screening_priors(client):

    episodes = []

    sorted_episodes = sort_episodes(client)
    if not sorted_episodes:
        return episodes

    prev_episode = None
    for date, episode in sorted_episodes:

        if (episode.type == omidb.episode.Type.CI) and prev_episode:
            if is_screening_episode(prev_episode):
                episodes.append((episode, prev_episode))

        prev_episode = episode

    return episodes


def lesion_is_malignant(lesion):
    """
    Returns `True` if the lesion opinion is code `B5` or `H5`; `False`
    otherwise.
    """
    for event in (lesion.surgery, lesion.biopsy_wide):
        if event and event.opinion in (Opinion.B5, Opinion.H5):
            return True
    return False


def get_malignant_lesions(episode):
    lesions = []
    if not episode.lesions:
        return lesions

    for lesion_id, lesion in episode.lesions.items():
        if lesion_is_malignant(lesion):
            lesions.append(lesion)
    return lesions


def classify_lesion(lesion):
    if lesion.is_invasive:
        lesion_type = "Invasive"
    elif lesion.is_insitu:
        lesion_type = "Insitu"
    else:
        lesion_type = "NoType"

    grade_name = lesion.grade.name if lesion.grade else "NoGrade"

    return f"{lesion_type}-{grade_name}"


def classify_malignant_lesions_where_screening_imaging(client):

    # CI episodes for which there are prior screening episodes
    episodes = get_ci_episodes_with_screening_priors(client)
    if episodes:
        episodes = [_[0] for _ in episodes]  # take CI episode (not prior)
    else:
        episodes = []

    # Add remaining episodes with screening imaging
    for episode in client.episodes:
        # Exclude CI episodes
        if episode.type == omidb.episode.Type.CI:
            continue

        # Not necessarily type R or F
        if episode_has_screening_images(episode) and episode.lesions:
            episodes.append(episode)

    # Now extract lesions for these episodes
    out = []
    for episode in episodes:
        lesions = get_malignant_lesions(episode)
        for lesion in lesions:
            lesion_class = classify_lesion(lesion)
            out.append(f"{episode.type.name}-{lesion_class}")

    return out


def parse_age(image):

    try:
        age_s = image.attributes["00101010"]["Value"][0]
        age = int(re.match(r"\d+", age_s.lstrip("0")).group())
        if age_s.endswith("M"):
            age /= 12
        if age_s.endswith("W"):
            age /= 52
        if age_s.endswith("D"):
            age /= 365
        return int(age)
    except Exception:
        return None
