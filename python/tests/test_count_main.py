from omidb_paper_stats import count_funcs


def test_main(path="/Users/deeuu/OMI-DB"):

    clients = ["demd410", "demd854", "demd891", "demd18181", "demd40555", "demd7585"]
    out = count_funcs.main(path, clients)

    episodes = out["ClientEpisode"]

    assert episodes["normal"].get(3) == 1
    assert episodes["malignant"].get(1) == 2
    assert episodes["benign"].get(1) == 1
    assert episodes["interval"].get(2) == 1

    lesions = out["ClassifiedMalignantLesions"]
    assert lesions.get("R-Invasive-G2") == 1
    assert lesions.get("R-Invasive-G1") == 1

    ages = out["Ages"]
    expected = [63, 66, 69, 64, 62, 59, 68, 65, 65, 68, 71, 61, 64]
    [ages["R"].remove(_) for _ in expected]
    assert not ages["R"]

    expected = [69, 66, 67]
    [ages["X"].remove(_) for _ in expected]
    assert not ages["X"]

    expected = [63]
    [ages["F"].remove(_) for _ in expected]
    assert not ages["F"]

    clients = out["ClientsWithScreeningImages"]
    assert clients["normal"] == 1
    assert clients["malignant"] == 2
    assert clients["benign"] == 1
    assert clients["interval"] == 2
